namespace tool {
 class IX;
}
namespace env {
 tool::IX* GetX();
}
namespace tool {
 class IX {
  public:
   virtual void Calc(int a, int b) = 0;
 };
}
