namespace net {
 class IBuffer;
 class ISocket;
 class IReactor;
 class IAccepter;
}
namespace env {
 net::ISocket* GetSocket();
 net::ISocket* GetSocket(int fd);
 net::IBuffer* GetBuffer();
 net::IReactor* GetReactor();
 net::IAccepter* GetAccepter();
}
namespace net {
class IEvent {
 public:
  virtual ~IEvent() {}
 public:
  virtual uint32_t GetFd() = 0;
  virtual void OnEvent(uint32_t event_type) = 0;
};
}
namespace net {
class IBuffer;
class ISocketHandler;
class NetAddress;
class ISocket {
 public:
  virtual ~ISocket() {}
 public:
  virtual void SetHandler(ISocketHandler* handler) = 0;
  virtual IBuffer* GetInBuffer() = 0;
  virtual IBuffer* GetOutBuffer() = 0;
  virtual uint32_t Connect(NetAddress& address) = 0;
  virtual uint32_t Send(const char* data, uint32_t len) = 0;
  virtual void Close() = 0;
};
}
namespace net {
class IBuffer {
 public:
  virtual ~IBuffer() {}
 public:
  virtual void Hold() = 0;
  virtual void Revert() = 0;
  virtual void Commit(uint32_t size = 0) = 0;
  virtual char* GetData() = 0;
  virtual uint32_t GetSize() = 0;
  virtual uint32_t Read(char* buf, uint32_t len) = 0;
  virtual uint32_t Write(char* buf, uint32_t len) = 0;
};
}
namespace net {
typedef enum {
 ET_READ = 1,
 ET_WRITE = 2,
}EVENT_TYPE;
}
namespace net {
class ITimer;
class IEvent;
class IReactor {
 public:
  virtual void Stop() = 0;
  virtual void Dispatch() = 0;
  virtual void RemoveEvent(IEvent* event) = 0;
  virtual void AddEvent(IEvent* event, uint32_t event_type) = 0;
  virtual void AddTimer(ITimer* timer) = 0;
};
}
namespace net {
class IAccepterHandler;
class NetAddress;
class IAccepter {
 public:
  virtual bool Listen(NetAddress& address) = 0;
  virtual void SetHandler(IAccepterHandler* handler) = 0;
};
}
namespace net {
class NetAddress {
 protected:
  uint32_t ip;
  uint32_t port;
  std::string address;
 public:
  NetAddress(uint32_t ip, uint32_t port);
  NetAddress(std::string ip, uint32_t port);
 public:
  uint32_t GetIp();
  std::string& GetSIp();
  uint32_t GetPort();
  uint32_t GetNetPort();
};
}
namespace net {
class ISocket;
class ISocketHandler {
 public:
  virtual ~ISocketHandler() {}
 public:
  virtual void OnRead(ISocket* socket) = 0;
  virtual void OnWrite(ISocket* socket) = 0;
  virtual void OnClose(ISocket* socket) = 0;
  virtual void OnConnected(ISocket* socket) = 0;
  virtual void OnError(ISocket* socket, uint32_t error) = 0;
};
}
namespace net {
class ISocket;
class IAccepterHandler {
 public:
  virtual void OnAccept(ISocket* socket) = 0;
};
}
