PROJECT_NAME="test"

BIN_PATH=`dirname $0`

cd $BIN_PATH

if [ ! -d "../logs" ]; then
	mkdir ../logs
fi

if [ ! -d "../config" ]; then
	mkdir ../config
fi

ulimit -c unlimited #core文件的大小

Debug() {
	cgdb ./$PROJECT_NAME
}

Start() {
	./$PROJECT_NAME
}

Error() {
	echo "error param [debug | start]"
}

if [ $# -lt 1 ]; then
	Error
	exit 1
fi

if [ $1 == "debug" ]; then
	Debug
elif [ $1 == "start" ]; then
	Start
else
	Error
fi

