#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

#include <map>
#include <set>
#include <list>
#include <queue>
#include <string>

typedef unsigned char uint8_t;
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
#endif

