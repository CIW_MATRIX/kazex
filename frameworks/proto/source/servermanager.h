#ifndef _PROTO_SERVERMANAGER_H_
#define _PROTO_SERVERMANAGER_H_

#ifndef API

#include "pack/pack.h"
#include "command/typedef.h"

#endif

namespace proto {

/* Server -> ServerManager */
/* 每1个Server在开始的时候，都会向ServerManager服务器发送该请求 */
class PRegisterServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		UInt32 server_type; 

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class RRegisterServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		bool is_success;

		UInt32 ip;
		UInt32 port; 
		UInt32 server_id; 

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

/* 当ServerManager挂了，需要恢复Server的状态 */
class PRecoverServer {

};

class RRecoverServer {

};

class PWatcherServer : public pack::IPacket {
	public:
		typedef std::list<UInt32> WATCHER_LIST;

	public:
		enum { URI = 1 };

	public:
		WATCHER_LIST watcher_list;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class RWatcherServer : public pack::IPacket {
	public:
		typedef std::list<app::pack::ServerInfo> SERVER_LIST;

	public:
		enum { URI = 1 };

	public:
		SERVER_LIST server_list;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class PServerAdd : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		pack::ServerInfo server_info;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class PServerRemove: public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		pack::ServerInfo server_info;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

/* 查询1台Server的信息(需要Server的IP和Port） */
class PQueryServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		UInt32 ip;
		UInt32 port;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

/* ServerManager返回的Server信息:
 * A.is_server表示该Server是否是1台合法(已经向ServerManager注册)的
*/
class RQueryServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		bool is_server;

		pack::ServerInfo server_info;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class PStartServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		pack::ServerInfo server_info;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class RStartServer {

};

class PStopServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		pack::ServerInfo server_info;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

class RStopServer {

};

class PPingServer : public pack::IPacket {
	public:
		enum { URI = 1 };

	public:
		pack::ServerInfo server_info;

	public:	
		virtual uint32_t GetUri();

		virtual bool Pack(pack::Writer& writer);
		virtual bool UnPack(pack::Reader& reader);
};

}

#endif
