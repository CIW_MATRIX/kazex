#include "writer.h"

namespace pack {

Writer::Writer(net::IBuffer* buffer) {
	this->buffer = buffer;
}

Writer::~Writer() {

}

Writer& Writer::operator<<(uint32_t v) {
	buffer->Write((char*)&v, sizeof(uint32_t));
	return *this;
}

Writer& Writer::operator<<(std::string& v) {
	size_t size = v.size();

	buffer->Write((char*)&size, sizeof(size_t));
	buffer->Write((char*)v.c_str(), size);
	return *this;
}

Writer& Writer::operator<<(std::list<uint32_t>& v) {
	size_t size = v.size();
	buffer->Write((char*)&size, sizeof(size_t));

	for(typename::std::list<uint32_t>::iterator it = v.begin(); it != v.end(); it++) {
		this->operator<<(*it);
	}
	return *this;
}

Writer& Writer::operator<<(std::list<std::string>& v) {
	size_t size = v.size();
	buffer->Write((char*)&size, sizeof(size_t));

	for(typename::std::list<std::string>::iterator it = v.begin(); it != v.end(); it++) {
		this->operator<<(*it);
	}
	return *this;
}

}
