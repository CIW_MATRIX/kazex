#ifndef _PACK_REQUEST_H_
#define _PACK_REQUEST_H_

#ifndef API
#include "net/net.h"
#endif

namespace pack {

class IPacket;

class Request {
	protected:
		uint32_t uri;
		uint32_t size;

	protected:
		net::IBuffer* buffer;

	public:
		Request(net::IBuffer* buffer);
		~Request();

	public:
		bool Pack(IPacket* packet);
		bool UnPack(IPacket* packet);
};
	
}
#endif
