#include "ipacket.h"

#include "reader.h"
#include "writer.h"
#include "request.h"

namespace pack {

Request::Request(net::IBuffer* buffer) {
	this->buffer = buffer;
}

Request::~Request() {

}

bool Request::Pack(IPacket* packet) {
	net::IBuffer* temp_buffer = env::GetBuffer();
	Writer writer(buffer);
	Writer temp_writer(temp_buffer);

	//1.先打包到temp中去，以便获取packet的大小
	packet->Pack(temp_writer);

	//2.获取packet的uri和size
	uri = packet->GetUri();
	size = temp_buffer->GetSize() + 4; //4 = (uri的大小)

	//3.真正打包packet
	writer<<size<<uri;
	packet->Pack(writer);

	delete temp_buffer;
	return true;
}

bool Request::UnPack(IPacket* packet) {
	uint32_t uri = 0;
	uint32_t size = 0;
	bool is_unpack = false;
	Reader reader(buffer);

	if(buffer->GetSize() < 8) { //8 = (size + uri的大小)
		return false;
	}

	//1.锁住缓存的位置
	buffer->Hold();

	//2.读取size, 并判断是否是一个完整的包
	reader>>size;
	if(buffer->GetSize() < size) {
		buffer->Revert(); //不是一个完整的包,Revert掉
		return false;
	}

	//3.先读取uri，再Unpack整个包
	reader>>uri;
	is_unpack = packet->UnPack(reader);
	if(is_unpack) {
		buffer->Commit(0); //成功解包，Commit缓存
	}
	else {
		buffer->Revert();
	}

	return is_unpack;
}

}
