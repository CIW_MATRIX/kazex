#ifndef _PACK_STRINGBUFFER_H_
#define _PACK_STRINGBUFFER_H_

#ifndef API
#include "net/net.h"
#endif

namespace pack {

class StringBuffer : public net::IBuffer {
	public:
		virtual void Hold();
		virtual void Commit();
		virtual void Revert();

		virtual char* GetData();
		virtual uint32_t GetSize();

		virtual uint32_t Read(char* buf, uint32_t len);
		virtual uint32_t Write(char* buf, uint32_t len);
};

}

#endif
