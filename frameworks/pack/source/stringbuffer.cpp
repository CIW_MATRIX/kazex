#include "stringbuffer.h"

namespace pack {

void StringBuffer::Hold() {

}

void StringBuffer::Commit() {

}

void StringBuffer::Revert() {

}

char* StringBuffer::GetData() {

}

uint32_t StringBuffer::GetSize() {

}

uint32_t StringBuffer::Read(char* buf, uint32_t len) {

}

uint32_t StringBuffer::Write(char* buf, uint32_t len) {

}

}
