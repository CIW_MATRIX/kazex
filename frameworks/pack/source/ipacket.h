#ifndef _PACK_IPACKET_H_
#define _PACK_IPACKET_H_

#ifndef API
#include "net/net.h"
#endif

namespace pack {

class Reader;
class Writer;

class IPacket {
	public:
		virtual uint32_t GetUri() = 0;

		virtual bool Pack(Writer& writer) = 0;
		virtual bool UnPack(Reader& reader) = 0;
};

}

#endif
