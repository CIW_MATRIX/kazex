#ifndef _TOOL_ILOGGER_H_
#define _TOOL_ILOGGER_H_

#ifndef API 

#include "common/typedef.h"

#endif

typedef enum {
	LOG_INFO,
	LOG_ALERT,
	LOG_DEBUG,
	LOG_ERROR,
}LOG_LEVEL;

namespace tool {

class ILogger {
	public:
		virtual uint32_t SetLevel(LOG_LEVEL level) = 0;
		virtual uint32_t WriteLog(const char* function, const char* format, ...) = 0;
};

}

#endif
