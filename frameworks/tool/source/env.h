#ifndef _ENV_ENV_H_
#define _ENV_ENV_H_

namespace tool {

class IConfigFile;

}

namespace env {

tool::IConfigFile* GetConfigFile();

}

#endif
