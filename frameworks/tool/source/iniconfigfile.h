#ifndef _TOOL_INICONFIGFILE_H_
#define _TOOL_INICONFIGFILE_H_

#include <cstdlib>
#include <cstring>

#include <map>
#include <string>
#include <fstream>
#include <iostream>

#include "iconfigfile.h"

namespace tool {

class INIConfigFile : public IConfigFile {
	public:
		typedef std::list<std::string> VALUE_LIST;
		typedef std::map<std::string, VALUE_LIST> VALUE_MAP; //<Key, Value>

	public:
		bool is_open;
		std::ifstream in;
		VALUE_MAP value_map;

	public:
		INIConfigFile();
		~INIConfigFile();

		bool IsLoad();
		uint32_t Load(std::string filename);
		uint32_t GetValue(std::string key, uint32_t& v);
		uint32_t GetValue(std::string key, std::string& v);
		uint32_t GetValue(std::string key, std::list<uint32_t>& v);
		uint32_t GetValue(std::string key, std::map<uint32_t, uint32_t>& v);
		uint32_t GetValue(std::string key, std::list<std::string>& v);
		uint32_t GetValue(std::string key, std::map<uint32_t, std::string>& v);

	protected:
		uint32_t GetValue(std::string key, uint32_t& v, int index);
		bool GetKeyValue(char* line, std::string& key, VALUE_LIST& value_list);
};

}
#endif
