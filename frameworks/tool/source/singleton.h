#ifndef _TOOL_SINGLETON_H_
#define _TOOL_SINGLETON_H_

namespace tool {
	
template<typename T>
class Singleton {
	public:
		static T* Instance();
};

}

#endif
