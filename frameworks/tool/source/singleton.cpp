#include "singleton.h"

namespace tool {

template<typename T>
T* Singleton<T>::Instance() {
	static T instance;
	return &instance;
}

}
