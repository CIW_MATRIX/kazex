#ifndef _TOOL_ICONFIGFILE_H_
#define _TOOL_ICONFIGFILE_H_

#ifndef API

#include <map>
#include <list>
#include <string>

#include "common/typedef.h"

#endif

namespace tool {

class IConfigFile {
		public:
			virtual bool IsLoad() = 0;

			virtual uint32_t Load(std::string filename) = 0;

			virtual uint32_t GetValue(std::string key, uint32_t& v) = 0;
			virtual uint32_t GetValue(std::string key, std::string& v) = 0;
			virtual uint32_t GetValue(std::string key, std::list<uint32_t>& v) = 0;
			virtual uint32_t GetValue(std::string key, std::list<std::string>& v) = 0;
			virtual uint32_t GetValue(std::string key, std::map<uint32_t, uint32_t>& v) = 0;
			virtual uint32_t GetValue(std::string key, std::map<uint32_t, std::string>& v) = 0;
};

}

#endif
