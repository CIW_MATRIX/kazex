#ifndef _NET_TCPSOCKET_H_
#define _NET_TCPSOCKET_H_

#include "common/typedef.h"

#include "isocket.h"

namespace net {
	const uint32_t MAX_TCPSOCKET_BUFFER = 1024 * 32;

	typedef enum {
		SS_NORMAL,
		SS_CLOSED,	
		SS_ERROR,
	}SOCKET_STATE;
}

namespace net {

class IBuffer;
class IReactor;
class ISocketHandler;

class TCPSocket : public ISocket , public IEvent {
	protected:
		uint32_t fd;
		uint32_t error;
		bool is_connected;
		SOCKET_STATE state;

	protected:
		IReactor* reactor;
		IBuffer* in_buffer;
		IBuffer* out_buffer;
		ISocketHandler* handler;

	public:
		TCPSocket();
		TCPSocket(uint32_t fd);
		~TCPSocket();

	private:
		void Init();
		bool SetBlock(bool is_block);

		void OnRead();
		void OnWrite();
		void OnConnect();

		void Send();
		void Error();
		void Destroy();

	public:
		virtual void SetHandler(ISocketHandler* handler);

		virtual IBuffer* GetInBuffer();
		virtual IBuffer* GetOutBuffer();

		virtual uint32_t Connect(NetAddress& address);
		virtual uint32_t Send(const char* data, uint32_t len);

		virtual void Close();

	protected:
		virtual uint32_t GetFd();
		virtual void OnEvent(uint32_t event_type);
};

}
#endif
