#ifndef _NET_IBUFFER_H_
#define _NET_IBUFFER_H_

#ifndef API
#include "common/typedef.h"
#endif

namespace net {

class IBuffer {
	public:
		virtual ~IBuffer() {}

	public:
		virtual void Hold() = 0;   //锁住内存的当前位置
		virtual void Revert() = 0; //还原内存Hold的位置
		virtual void Commit(uint32_t size = 0) = 0; //提交当前内存的位置

		virtual char* GetData() = 0;
		virtual uint32_t GetSize() = 0;

		virtual uint32_t Read(char* buf, uint32_t len) = 0;
		virtual uint32_t Write(char* buf, uint32_t len) = 0;

};

}
#endif
