#include "env.h"
#include "tcpsocket.h"
#include "flatbuffer.h"
#include "tcpaccepter.h"
#include "epollreactor.h"

namespace env {

net::ISocket* GetSocket() {
	return new net::TCPSocket();
}

net::ISocket* GetSocket(int fd) {
	return new net::TCPSocket(fd);
}

net::IBuffer* GetBuffer() {
	return new net::FlatBuffer();
}

net::IReactor* GetReactor() {
	static net::EpollReactor instance;
	return &instance;
}

net::IAccepter* GetAccepter() {
	return new net::TCPAccepter();
}

}
