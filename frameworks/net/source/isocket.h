#ifndef _NET_ISOCKET_H_
#define _NET_ISOCKET_H_

#ifndef API
#include "common/typedef.h"

#include "ievent.h"
#endif

namespace net {

class IBuffer;
class ISocketHandler;

class NetAddress;

class ISocket {
	public:
		virtual ~ISocket() {}

	public:
		virtual void SetHandler(ISocketHandler* handler) = 0;

		virtual IBuffer* GetInBuffer() = 0;
		virtual IBuffer* GetOutBuffer() = 0;

		virtual uint32_t Connect(NetAddress& address) = 0;
		virtual uint32_t Send(const char* data, uint32_t len) = 0;

		virtual void Close() = 0;
};

}
#endif
