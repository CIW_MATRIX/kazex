#ifndef _NET_FLATBUFFER_H_
#define _NET_FLATBUFFER_H_

#include "common/typedef.h"

#include "ibuffer.h"

/*
 * 平滑的Buffer
 * 在初始化的时候，会预分配一片空间:
 * 1.在写入的时候，如果缓冲区太小，会扩大内存
 * 2.在读取掉数据的时候，如果以前的数据太多，会把数据向前移动
 */
namespace net  {
	const uint32_t MIN_FLAT_BUFEER_SIZE = 32;
}

namespace net {

class FlatBuffer : public IBuffer {
	protected:
		uint32_t size;		 //缓冲区大小
		uint32_t cur_pos;	 //当前的位置
		uint32_t blank_size; //最前面空白的大小
		uint32_t total_size; //整一片内存的大小

	protected:
		uint32_t hold_pos;	 
		uint32_t hold_size;  
		uint32_t hold_blank_size; 

	protected:
		char* buf;
		
	public:
		FlatBuffer();
		~FlatBuffer();

	protected:
		void Init();
		void Realloc(uint32_t len);
		void Copy(char* buf, uint32_t len);

	public:
		virtual void Hold();
		virtual void Revert();
		virtual void Commit(uint32_t size);

		virtual char* GetData();

		virtual uint32_t GetSize();

		virtual uint32_t Read(char* buf, uint32_t len);
		virtual uint32_t Write(char* buf, uint32_t len);
};

}
#endif
