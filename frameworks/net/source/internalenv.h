#ifndef _ENV_INTERNALENV_H_
#define _ENV_INTERNALENV_H_

#include "env.h"

namespace net {
	class IBuffer;
}

namespace env {
	net::IBuffer* GetBuffer();
}
#endif
