#include "internalenv.h"

#include "flatbuffer.h"

namespace env {

net::IBuffer* GetBuffer() {
	return new net::FlatBuffer();
}

}
