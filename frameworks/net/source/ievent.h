#ifndef _NET_IEVENT_H_
#define _NET_IEVENT_H_

#ifndef API
#include "common/typedef.h"
#endif

namespace net {

class IEvent {
	public:
		virtual ~IEvent() {}

	public:
		virtual uint32_t GetFd() = 0;
		virtual void OnEvent(uint32_t event_type) = 0;
};

}
#endif
