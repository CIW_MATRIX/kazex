#include "netaddress.h"

#include <arpa/inet.h>

namespace net {

NetAddress::NetAddress(uint32_t ip, uint32_t port) {
	this->ip = ip;
	this->port = port;
}

NetAddress::NetAddress(std::string ip, uint32_t port) {
	this->port = port;
	this->address = ip;
	this->ip = inet_addr(ip.c_str());
}

uint32_t NetAddress::GetIp() {
	return ip;
}

std::string& NetAddress::GetSIp() {
	return address;
}

uint32_t NetAddress::GetPort() {
	return port;
}

uint32_t NetAddress::GetNetPort() {
	return htons(port);
}

}
