#ifndef _NET_ITIMER_H_
#define _NET_ITIMER_H_

#include "common/typedef.h"

namespace net {

class ITimerHandler;

class ITimer {
	public:
		virtual ~ITimer() {}

	public:
		virtual void Close() = 0;
		virtual void GetTime() = 0;
		virtual void Start(uint32_t time, ITimerHander* handler);
};

}
#endif
