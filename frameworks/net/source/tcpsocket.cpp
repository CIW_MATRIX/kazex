#include "ibuffer.h"
#include "ireactor.h"
#include "isockethandler.h"

#include "env.h"
#include "tcpsocket.h"
#include "netaddress.h"
#include "internalenv.h"

#include <errno.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

namespace net {
	char* tcpsocket_buffer = new char[MAX_TCPSOCKET_BUFFER];
}

namespace net {

TCPSocket::TCPSocket() {
	is_connected = false;

	fd = socket(AF_INET, SOCK_STREAM, 0);

	Init();
}

TCPSocket::TCPSocket(uint32_t fd) {
	this->fd = fd;

	Init();
}

TCPSocket::~TCPSocket() {
	if(fd != -1) {
		close(fd);
		fd = -1;
	}

	if(in_buffer) {
		delete in_buffer;
		in_buffer = NULL;
	}

	if(out_buffer) {
		delete out_buffer;
		out_buffer = NULL;
	}
}

void TCPSocket::Init() {
	assert(fd != -1);

	error = 0;
	handler = NULL;
	state = SS_NORMAL;

	//申请缓冲区
	in_buffer = env::GetBuffer();
	out_buffer = env::GetBuffer();

	//增加到Reactor中
	reactor = env::GetReactor();
	reactor->AddEvent(this, ET_READ | ET_WRITE);

	//非阻塞
	SetBlock(true);
}

void TCPSocket::SetHandler(ISocketHandler* handler) {
	this->handler = handler;
}

IBuffer* TCPSocket::GetInBuffer() {
	return in_buffer;
}

IBuffer* TCPSocket::GetOutBuffer() {
	return out_buffer;
}

bool TCPSocket::SetBlock(bool is_block) {
	int flag = 0;

	flag = fcntl(fd, F_GETFL);
	if(flag == -1) {
		return false;
	}

	if(is_block) {
		flag = fcntl(fd, F_SETFL, flag | O_NONBLOCK);
	}
	else {
		flag = fcntl(fd, F_SETFL, flag & (~O_NONBLOCK));
	}

	if(flag == -1) {
		return false;
	}

	return true;
}

uint32_t TCPSocket::Connect(NetAddress& address) {
	int ret = 0;
	socklen_t len = 0;
	sockaddr_in addr = {0};

re_connect:
	addr.sin_family = AF_INET;
	addr.sin_port = address.GetNetPort();
	addr.sin_addr.s_addr = inet_addr(address.GetSIp().data());

	len = sizeof(sockaddr_in);
	ret = connect(fd, (sockaddr*)&addr, len);
	if(ret == -1) {
		if(errno == EAGAIN) {
			goto re_connect;
		}
	}
}

uint32_t TCPSocket::Send(const char* data, uint32_t len) {
	out_buffer->Write((char*)data, len);	
	Send();
	return true;
}

void TCPSocket::Close() {
	state = SS_CLOSED;

	Destroy();
}

void TCPSocket::Error() {
	error = errno;
	state = SS_ERROR;

	Destroy();
}

void TCPSocket::Destroy() {
	if(state == SS_CLOSED) {
		if(handler) {
			handler->OnClose(this);
		}
	}
	else if(state == SS_ERROR) {
		if(handler) {
			handler->OnError(this, error);
		}
	}

	delete this;
}

uint32_t TCPSocket::GetFd() {
	return fd;
}

void TCPSocket::OnEvent(uint32_t event_type) {
	if(handler == NULL) {
		Destroy();
		return ;
	}

	if(state != SS_NORMAL) {
		Destroy();
		return ;
	}

	if(event_type & EPOLLRDHUP) {
		Close();
		return ;
	}

	if(event_type & EPOLLOUT) {
		OnWrite();
	}

	if(event_type & EPOLLIN) {
		OnRead();
	}
}

void TCPSocket::OnRead() {
	int ret = 0;

	while(1) {
		ret = recv(fd, tcpsocket_buffer, MAX_TCPSOCKET_BUFFER, 0);

		if(ret > 0) {
			in_buffer->Write(tcpsocket_buffer, ret);
		}
		else if(ret == 0) {
			Close();
		}
		else {
			if(errno == EAGAIN || errno == EWOULDBLOCK) {
				break; 
			}

			Error();
		}
	}

	if(state == SS_NORMAL) {
		handler->OnRead(this);
	}
}

void TCPSocket::OnWrite() {
	if(is_connected == false) {
		OnConnect();
	}
	else {
		Send();
	}
}

void TCPSocket::Send() {
	int ret = 0;
	int remain = 0;

	while(1) {
		//1.读取MAX_TCPSOCKET_BUFFER字节数据
		out_buffer->Hold();
		remain = out_buffer->Read(tcpsocket_buffer, MAX_TCPSOCKET_BUFFER);
		if(remain == 0) {
			break;
		}

		//2.把它发送出去
		ret = send(fd, tcpsocket_buffer, remain, 0);
		if(ret > 0) {
			out_buffer->Commit(ret); //把成功发送的内存提交了
		}

		if(ret == -1) {
			if(errno == EWOULDBLOCK) {
				break;
			}

			if(errno == EAGAIN) {
				continue;
			}
			else {
				Error();
			}

			/*if(errno == ECONNRESET || errno == EPIPE) {
				Error();
			}*/
		}
	}

	if(state == SS_NORMAL) {
		handler->OnWrite(this);
	}
}

void TCPSocket::OnConnect() {
	int error = 0;
	socklen_t len = sizeof(int);

	if(getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &len) == 0) {
		if(error == 0) {
			is_connected = true;

			handler->OnConnected(this);
		}
		else {
			handler->OnError(this, error);
			Close();
		}
	}
}

}
