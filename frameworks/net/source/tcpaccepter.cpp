#include "ireactor.h"
#include "iaccepterhandler.h"

#include "netaddress.h"
#include "tcpaccepter.h"
#include "internalenv.h"

#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <iostream>

namespace net {

TCPAccepter::TCPAccepter() {
	reactor = env::GetReactor();

	fd = socket(AF_INET, SOCK_STREAM, 0);
	SetBlock(true);
}

TCPAccepter::~TCPAccepter() {
	if(fd != -1) {
		close(fd);
		fd = -1;
	}
}

void TCPAccepter::SetHandler(IAccepterHandler* handler) {
	this->handler = handler;
}

bool TCPAccepter::Listen(NetAddress& address) {
	char* error = NULL;
	int ret = true;
	int optval = 1;
	sockaddr_in addr = {0};

	assert(fd != -1);		
	assert(reactor != NULL);		

	addr.sin_family = AF_INET;
	addr.sin_port = address.GetNetPort();
	addr.sin_addr.s_addr = address.GetIp();

	//1.监听
	ret = bind(fd, (sockaddr*)&addr, sizeof(sockaddr));
	assert(ret != -1);

	ret = listen(fd, MAX_LISTEN_SIZE);
	assert(ret != -1);

	ret = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

	//2.增加到Reacotr中
	reactor->AddEvent(this, ET_READ);
	assert(ret != -1);
	return true;
}

uint32_t TCPAccepter::GetFd() {
	return fd;
}

void TCPAccepter::OnEvent(uint32_t event_type) {
	int client = -1;
	ISocket* socket = NULL;

	while(1) {
		client = accept(fd, NULL, 0);
		if(client == -1) {
			//assert(false);
			return ;
		}

		socket = env::GetSocket(client);
		if(handler) {
			handler->OnAccept(socket);
		}
	}
}

bool TCPAccepter::SetBlock(bool is_block) {
	int flag = 0;

	flag = fcntl(fd, F_GETFL);
	if(flag == -1) {
		return false;
	}

	if(is_block) {
		flag = fcntl(fd, F_SETFL, flag | O_NONBLOCK);
	}
	else {
		flag = fcntl(fd, F_SETFL, flag & (~O_NONBLOCK));
	}

	if(flag == -1) {
		return false;
	}

	return true;
}

}
