#ifndef _ENV_ENV_H_
#define _ENV_ENV_H_

namespace net {
	class IBuffer;
	class ISocket;
	class IReactor;
	class IAccepter;
}

namespace env {
	net::ISocket* GetSocket();
	net::ISocket* GetSocket(int fd);

	net::IBuffer* GetBuffer();

	net::IReactor* GetReactor();

	net::IAccepter* GetAccepter();
}

#endif
