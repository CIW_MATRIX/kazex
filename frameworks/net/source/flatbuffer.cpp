#include "flatbuffer.h"

#include <assert.h>
#include <string.h>

namespace net {

FlatBuffer::FlatBuffer() {
	Init();

	total_size = MIN_FLAT_BUFEER_SIZE;

	hold_pos = 0;
	hold_size = 0;
	hold_blank_size = 0;

	buf = new char[total_size];
}

FlatBuffer::~FlatBuffer() {
	if(buf) {
		delete[] buf;
		buf = NULL;
	}
}

void FlatBuffer::Init() {
	size = 0;
	cur_pos = 0; 
	blank_size = 0;
}				

char* FlatBuffer::GetData() {
	assert(false);
}

uint32_t FlatBuffer::GetSize() {
	return size;
}

void FlatBuffer::Hold() {
	//assert(hold_pos == 0);
	//assert(hold_size == 0);
	//assert(hold_blank_size == 0);

	hold_size = size;
	hold_pos = cur_pos;
	hold_blank_size = blank_size;
}

void FlatBuffer::Commit(uint32_t size) {
	uint32_t real_size = size;

	if(size == 0) { //size = 0, 表示整一片内存全部提交
		real_size = this->hold_size;
	}

	assert(this->hold_size >= size);

	if(this->hold_size < real_size) {
		real_size = this->hold_size;
	}

	cur_pos = hold_pos + real_size;
	this->size = hold_size - real_size;
	blank_size = hold_blank_size + real_size;

	if(this->size == 0) { 
		Init();
	}
}

void FlatBuffer::Revert() {
	size = hold_size;
	cur_pos = hold_pos;
	blank_size = hold_blank_size;
}

uint32_t FlatBuffer::Read(char* buf, uint32_t len) {
	uint32_t real_size = len;

	if(buf == NULL || len == 0 || size == 0) {
		return 0;
	}

	if(real_size > size) {
		real_size = size;
	}

	memcpy(buf, this->buf + cur_pos, real_size);

	size -= real_size;

	if(size == 0) { 
		Init();
	}
	else {
		cur_pos += real_size;
		blank_size += real_size;
	}
	return real_size;
}

uint32_t FlatBuffer::Write(char* buf, uint32_t len) {
	if(buf == NULL || len == 0) {
		return 0;
	}

	if(len <= (total_size - size - blank_size)) { //剩下的空间可以写下
		Copy(buf, len);
	}
	else {
		if(blank_size >= len) { //前面空隙的大小可以容纳，需要移动内存
			memmove(this->buf, this->buf + cur_pos, size);
			cur_pos -= blank_size;
			blank_size = 0;

			Copy(buf, len);
		}
		else { //内存不够，需要扩容
			Realloc(len);				
			Copy(buf, len);
		}
	}

	size += len;
	return len;
}

void FlatBuffer::Realloc(uint32_t len) {
	char* buf = NULL;
	int real_size = total_size;

	for(int i = 0; i < 99999999; i++) {
			real_size *= 2;	//2倍扩增
			if(real_size >= len) {
				break;
			}
	}

	//1.申请新的内存
	buf = new char[real_size];

	//2.把旧的内存复制到新的内存中去
	memcpy(buf, this->buf + cur_pos, size);

	//3.释放旧的内存
	delete[] this->buf;

	//4.重置缓冲区的信息
	cur_pos = 0;
	blank_size = 0;	
	this->buf = buf;
	total_size = real_size;
}

void FlatBuffer::Copy(char* buf, uint32_t len) {
	memcpy(this->buf + cur_pos + size, buf, len);
}

}
