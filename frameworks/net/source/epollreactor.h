#ifndef _NET_EPOLLREACTOR_H_
#define _NET_EPOLLREACTOR_H_

#include "ireactor.h"

#include "common/typedef.h"

#include <sys/epoll.h>

namespace net {
	const uint32_t MAX_EPOLL_SIZE = 1024;
}

namespace net {

class EpollReactor : public IReactor {
	protected:
		int epoll_fd;

	protected:
		epoll_event events[MAX_EPOLL_SIZE];

	public:
		EpollReactor();
		~EpollReactor();

	public:
		void OnEvent(uint32_t count);
		void OnTimeout(ITimer* timer);

	public:
		virtual void Stop();
		virtual void Dispatch();
		virtual void RemoveEvent(IEvent* event);
		virtual void AddEvent(IEvent* event, uint32_t event_type);	

		virtual void AddTimer(ITimer* timer);
};

}
#endif
