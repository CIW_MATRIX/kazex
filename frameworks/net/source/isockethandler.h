#ifndef _NET_ISOCKETHANDLER_H_
#define _NET_ISOCKETHANDLER_H_

#ifndef API
#include "common/typedef.h"
#endif

/*
 * Socket提供给外面的接口
 */
namespace net {

class ISocket;

class ISocketHandler {
	public:
		virtual ~ISocketHandler() {}

	public:
		virtual void OnRead(ISocket* socket) = 0;
		virtual void OnWrite(ISocket* socket) = 0;

		virtual void OnClose(ISocket* socket) = 0;
		virtual void OnConnected(ISocket* socket) = 0;

		virtual void OnError(ISocket* socket, uint32_t error) = 0;
};

}
#endif
