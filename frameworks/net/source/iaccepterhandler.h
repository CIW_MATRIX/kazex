#ifndef _NET_ACCEPTRTHANDER_H_
#define _NET_ACCEPTRTHANDER_H_

namespace net {

class ISocket;

class IAccepterHandler {
	public:
		virtual void OnAccept(ISocket* socket) = 0;
};

}
#endif
