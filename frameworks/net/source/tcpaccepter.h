#ifndef _NET_TCPACCEPTER_H_
#define _NET_TCPACCEPTER_H_

#include "common/typedef.h"

#include "ievent.h"
#include "iaccepter.h"

namespace net {
	const uint32_t MAX_LISTEN_SIZE = 1024;
}

namespace net {

class IAccepterHandler;

class TCPAccepter : public IAccepter, public IEvent {
	protected:
		int fd;

	protected:
		IReactor* reactor;
		IAccepterHandler* handler;

	public:
		TCPAccepter();
		~TCPAccepter();

	protected:
		bool SetBlock(bool is_block);

	public:
		virtual bool Listen(NetAddress& address);
		virtual void SetHandler(IAccepterHandler* handler);

	protected:
		virtual uint32_t GetFd();
		virtual void OnEvent(uint32_t event_type);
};

}
#endif
