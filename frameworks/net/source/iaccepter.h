#ifndef _NET_IACCEPTER_H_
#define _NET_IACCEPTER_H_

namespace net {

class IAccepterHandler;

class NetAddress;

class IAccepter {
	public:
		virtual bool Listen(NetAddress& address) = 0;

		virtual void SetHandler(IAccepterHandler* handler) = 0;
};

}
#endif
