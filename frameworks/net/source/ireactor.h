#ifndef _NET_IREACTOR_H_
#define _NET_IREACTOR_H_

#ifndef API
	#include "common/typedef.h"
#endif

namespace net {

typedef enum {
	ET_READ = 1,
	ET_WRITE = 2,
}EVENT_TYPE;

}

namespace net {

class ITimer;
class IEvent;

class IReactor {
	public:
		virtual void Stop() = 0;
		virtual void Dispatch() = 0;
		virtual void RemoveEvent(IEvent* event) = 0;
		virtual void AddEvent(IEvent* event, uint32_t event_type) = 0;	

		virtual void AddTimer(ITimer* timer) = 0;
};

}
#endif
