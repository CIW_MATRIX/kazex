#ifndef _NET_NETADDRESS_H_
#define _NET_NETADDRESS_H_

#ifndef API
#include "common/typedef.h"

#include <string>
#endif

namespace net {

class NetAddress {
	protected:
		uint32_t ip;
		uint32_t port;
		std::string address;

	public:
		NetAddress(uint32_t ip, uint32_t port);
		NetAddress(std::string ip, uint32_t port);

	public:
		uint32_t GetIp();
		std::string& GetSIp();

		uint32_t GetPort();
		uint32_t GetNetPort();
};

}
#endif
