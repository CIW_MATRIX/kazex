#include "ievent.h"

#include "epollreactor.h"

#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

namespace net {
EpollReactor::EpollReactor() {
	epoll_fd = epoll_create(MAX_EPOLL_SIZE);

	assert(epoll_fd != -1);
}

EpollReactor::~EpollReactor() {
	if(epoll_fd != -1) {
		close(epoll_fd);
		epoll_fd = -1;
	}
}

void EpollReactor::Stop() {

}

void EpollReactor::Dispatch() {
	uint32_t ret = 0;

	while(1) {
		ret = epoll_wait(epoll_fd, events, MAX_EPOLL_SIZE, -1);
		if(ret == -1) {		
			if(errno == EINTR) {
				continue;	 //Error
			}
		}
		else if(ret == 0) { 
			OnTimeout(NULL); //Timeout
		}
		else {				
			OnEvent(ret);	 //Event
		}
	}
}

void EpollReactor::OnEvent(uint32_t count) {
	IEvent* event = 0;

	for(uint32_t i = 0; i < count; i++) {
		event = (IEvent*)events[i].data.ptr;

		event->OnEvent(events[i].events); //分派
	}
}

void EpollReactor::OnTimeout(ITimer* timer) {

}

void EpollReactor::RemoveEvent(IEvent* event) {

}

void EpollReactor::AddEvent(IEvent* event, uint32_t event_type) {
	char* error = NULL;
	int ret = 0;
	epoll_event epoll_event = {0};

	epoll_event.events = EPOLLET | EPOLLRDHUP; //ET模式

	if(event_type & ET_READ) {
		epoll_event.events |= EPOLLIN;
	}

	if(event_type & ET_WRITE) {
		epoll_event.events |= EPOLLOUT;
	}

	epoll_event.data.ptr = (void*)event;

	ret = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, event->GetFd(), &epoll_event);
	error = strerror(errno);

	assert(ret != -1);
}

void EpollReactor::AddTimer(ITimer* timer) {

}

}
