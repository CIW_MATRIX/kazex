#ifndef _APP_SERVERINFO_H_
#define _APP_SERVERINFO_H_

#include <list>

namespace app {

class ServerInfo {
	public:
		typedef std::list<UInt32> PORT_LIST; /* 1台Server可能有N个服务器 */

	public:
		UInt32 sid;
		UInt32 type;

		UInt32 ip;
		PORT_LIST port_list;
};

}

#endif
