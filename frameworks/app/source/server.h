#ifndef _APP_SERVER_H_
#define _APP_SERVER_H_

#include "net/net.h"

#include "iserver.h"

namespace app {

class Server : public IServer, public net::ISocketHandler {
	private:
		uint32_t server_type;
	
	private:
		IServerHandler* handler;
		net::ISocket* server_manager; /* 和ServerManager的连接 */

	public:
		Server();
		~Server();

	private: /* IServer */
		virtual void Run();		
		virtual void Terminate(); 

		virtual void SetHandler(IServerHandler* handler);

		virtual uint32_t GetId();

		virtual uint32_t GetServerType();
		virtual void SetServerType(uint32_t server_type);

	private: /* ISocketHandler */
		virtual void OnRead(net::ISocket* socket);
		virtual void OnWrite(net::ISocket* socket);

		virtual void OnClose(net::ISocket* socket);
		virtual void OnConnected(net::ISocket* socket);

		virtual void OnError(net::ISocket* socket, uint32_t error);
};

}

#endif
