#ifndef _APP_ICONNECTIONHANDLER_H_
#define _APP_ICONNECTIONHANDLER_H_

#include "commond/typedef.h"

namespace app {

class Request;

class IConnectionHandler {
	public:
		virtual void OnRequest(Request& request) = 0;

		virtual void OnClose(IConnection* connection) = 0;
		virtual void OnConnection(IConnection* connection) = 0;

		virtual void OnError(IConnection* connection, UInt32 error) = 0;
};

}

#endif
