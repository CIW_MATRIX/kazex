#ifndef _APP_ISERVER_H_
#define _APP_ISERVER_H_

#ifndef API

#include "common/typedef.h"

#include <string>

#endif

/*
 * 表示1个Server，每一个Server:
 * A.1个ServerType, Id
*/
namespace app {

class IServerHandler;

class IServer {
	public:
		virtual void Run() = 0;		  /* 开始Server */
		virtual void Terminate() = 0; /* 结束Server */

		virtual void SetHandler(IServerHandler* handler) = 0;

		virtual uint32_t GetId() = 0;

		virtual uint32_t GetServerType() = 0;
		virtual void SetServerType(uint32_t server_type) = 0;
};

}

#endif
