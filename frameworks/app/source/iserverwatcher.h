#ifndef _APP_ISERVERWATCHER_H_
#define _APP_ISERVERWATCHER_H_

#include "command/typedef.h"

#include <list>

namespace app {

typedef std::list<UInt32> WATCHER_LIST;

class IServerWatcher {
	public:
		virtual ~IServerWatcher() {}

		virtual void WatcherList(WATCHER_LIST& list) = 0;

		virtual void OnServerAdd(ServerInfo& server_info) = 0;
		virtual void OnServerRemove(ServerInfo& server_info) = 0;
};

}

#endif
