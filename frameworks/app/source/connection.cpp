#include "connection.h"

#include "pack/pack.h"

#include <cassert>

namespace app {

Connection::Connection(net::ISocket* socket) {
	this->socket = socket;
}

Connection::~Connection() {
	Close();
}

void Connection::Send(pack::IPacket* packet) {
	pack::Request out_request(socket->GetOutBuffer());

	out_request.UnPack(packet);
	socket->Send("", 0);
}

void Connection::Close() {
	if(socket) {
		socket->Close();
		socket = NULL;
	}
}

ConnectionInfo& Connection::GetConnectionInfo() {
	assert(false);
}

void Connection::AddHandler(IConnectionHandler* handler) {
	handler_set.insert(handler);
}

void Connection::RemoveHandler(IConnectionHandler* handler) {
	CONNECTION_HANDLER_SET::iterator it = handler_set.find(handler);

	if(it == handler_set.end()) {
		handler_set.erase(it);
	}
}

void Connection::OnRead(net::ISocket* socket) {

}

void Connection::OnWrite(net::ISocket* socket) {

}

void Connection::OnClose(net::ISocket* socket) {

}

void Connection::OnConnected(net::ISocket* socket) {
	
}


void Connection::OnError(net::ISocket* socket, uint32_t error) {

}

}
