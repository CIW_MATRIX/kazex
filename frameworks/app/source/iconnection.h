#ifndef _APP_ICONNECTION_H_
#define _APP_ICONNECTION_H_

#include "pack/pack.h"

namespace app {

class ConnectionInfo;
class IConnectionHandler;

class IConnection {
	public:
		virtual void Close() = 0;
		virtual void Send(pack::IPacket* packet) = 0;

		virtual ConnectionInfo& GetConnectionInfo() = 0;

		virtual void AddHandler(IConnectionHandler* handler) = 0;
		virtual void RemoveHandler(IConnectionHandler* handler) = 0;
};

}

#endif
