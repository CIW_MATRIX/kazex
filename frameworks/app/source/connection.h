#ifndef _APP_CONNECTION_H_
#define _APP_CONNECTION_H_

#include "net/net.h"

#include "iconnection.h"

#include <set>

namespace app {

class Connection : public IConnection, public net::ISocketHandler {
	public:
		typedef std::set<IConnectionHandler*> CONNECTION_HANDLER_SET;

	private:
		net::ISocket* socket;

	private:
		CONNECTION_HANDLER_SET handler_set;

	public:
		Connection(net::ISocket* socket);
		~Connection();

	public: /* IConnection */
		virtual void Close();
		virtual void Send(pack::IPacket* packet);

		virtual ConnectionInfo& GetConnectionInfo();

		virtual void AddHandler(IConnectionHandler* handler);
		virtual void RemoveHandler(IConnectionHandler* handler);

	private: /* net::ISocketHandler */
		virtual void OnRead(net::ISocket* socket);
		virtual void OnWrite(net::ISocket* socket);

		virtual void OnClose(net::ISocket* socket);
		virtual void OnConnected(net::ISocket* socket);

		virtual void OnError(net::ISocket* socket, uint32_t error);
};

}

#endif
