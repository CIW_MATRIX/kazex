#ifndef _APP_SERVERHANDLER_H_
#define _APP_SERVERHANDLER_H_

namespace app {

class IConnection;

class IServerHandler {
	public:
		virtual void Init() = 0;
		virtual void Stop() = 0;
		virtual void Start() = 0;

		virtual void OnInConnection(IConnection* connection) = 0;
		virtual void OnOutConnection(IConnection* connection) = 0;
};

}

#endif
