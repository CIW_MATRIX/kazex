#include "server.h"

namespace app {

Server::Server() {
	handler = NULL;

	server_manager = env::GetSocket();
}

Server::~Server() {

}

void Server::Run() {

}

void Server::Terminate() {

}

void Server::SetHandler(IServerHandler* handler) {
	this->handler = handler;
}

uint32_t Server::GetId() {
	
}

uint32_t Server::GetServerType() {

}

void Server::SetServerType(uint32_t server_type) {
	this->server_type = server_type;
}

void Server::OnRead(net::ISocket* socket) {

}

void Server::OnWrite(net::ISocket* socket) {

}

void Server::OnClose(net::ISocket* socket) {

}

void Server::OnConnected(net::ISocket* socket) {

}

void Server::OnError(net::ISocket* socket, uint32_t error) {

}

}
