#include "net/net.h" 

#include <iostream>

int main() {
	net::NetAddress address("127.0.0.1", 9999);
	net::IReactor* reactor = env::GetReactor();
	net::IAccepter* accepter = env::GetAccepter();

	accepter->Listen(address);
	reactor->Dispatch();
	return 0;
}
