#include "net/net.h" 
#include "pack/pack.h" 

#include <ctime>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

class HelloWorldPacket : public pack::IPacket {
	public:
		std::string str;	

	public:
		HelloWorldPacket() {
			str = "Hello, I am CIW_MATRIX.";
		}

		~HelloWorldPacket() {

		}

		uint32_t GetUri() {
			return 1;
		}

		bool Pack(pack::Writer& writer) {
			writer<<str;
			return true;
		}

		bool UnPack(pack::Reader& reader) {
			return reader>>str;
		}
};

class SocketHandler : public net::ISocketHandler {
	public:
		virtual void OnRead(net::ISocket* socket) {
			HelloWorldPacket packet;
			pack::Request in_request(socket->GetInBuffer());
			pack::Request out_request(socket->GetOutBuffer());

			while(in_request.UnPack(&packet)) {
				cout<<packet.str<<endl;
			}

			uint32_t n = rand() % 100000;
			packet.str = "";
			for(int i = 0; i < n; i++) {
				packet.str += "Client";
			}

			out_request.Pack(&packet);
			socket->Send("", 0);
		}

		virtual void OnWrite(net::ISocket* socket) {

		}

		virtual void OnClose(net::ISocket* socket) {

		}

		virtual void OnConnected(net::ISocket* socket) {
			HelloWorldPacket packet;
			pack::Request request(socket->GetOutBuffer());

			request.Pack(&packet);
			socket->Send("", 0);
		}

		virtual void OnError(net::ISocket* socket, uint32_t error) {

		}
};

int main() {
	SocketHandler handler;
	net::NetAddress address("127.0.0.1", 9999);
	net::IReactor* reactor = env::GetReactor();
	net::ISocket* socket = env::GetSocket();

	srand(time(NULL));

	socket->Connect(address);
	socket->SetHandler(&handler);

	reactor->Dispatch();
	return 0;
}
