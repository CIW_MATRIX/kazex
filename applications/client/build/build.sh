BUILD_PATH=`dirname $0`

cd $BUILD_PATH

if [ ! -d "./bin" ]; then
	mkdir "./bin"
fi

if [ ! -d "./config" ]; then
	mkdir ./config
fi

if [ ! -f "./bin/run.sh" ]; then #run运行脚本
	cp ../../../public/build/run.sh ./bin/run.sh 
fi

#执行cmake，并且编译程序
cmake .
make

